<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User ;

class LoginTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testLogin()
    {
        $response = $this->call('GET' , '/sis') ;
        $this->assertTrue(strpos($response->content() , 'Sign in') !== false) ;
        $response = $this->call('GET' , '/logout') ;
        $this->assertRedirectedTo('/sis') ;
        $response = $this->call('GET' , '/') ;
        $this->assertTrue(strpos($response->content() , 'Redirecting') !== false) ;
        $response = $this->call('GET' , '/dashboard');
        $this->assertTrue(strpos($response->content() , 'You need to login') !== false) ;
        $response = $this->call('GET' , '/print/102') ;
        $this->assertTrue(strpos($response->content() , 'You need to login') !== false) ;
        $response = $this->call('POST' , '/validate' , ['username' => 'yashwantetha' , 'password' => 'appocase007' , 'server'=>'Disang']) ;
        $this->assertTrue(strpos($response , '"success":false') !== false) ;
        $response = $this->call('POST' , '/search_results' , ['test' => 'Some text']) ;
        $this->assertTrue(strpos($response->content() , 'You need to login') !== false) ;
        $response = $this->call('GET' , '/departments/mech');
        $this->assertTrue(strpos($response->content() , 'You need to login') !== false) ;
        $response = $this->call('GET' , '/course/course_0');
    }
    public function testPostLogin()
    {
      $response = $this->call('POST' , '/validate' , ['username' => 'mahesh' , 'password' => 'chemilla' , 'server' => 'Disang']) ;
      $this->assertTrue(strpos($response , '"success":true') !== false);
      $response = $this->call('GET' , '/dashboard') ;
      $this->assertTrue(strpos($response , 'The past 5 students you viewed were') !== false);
      $response = $this->call('GET' , 'viewstudent/401') ;
      $this->assertTrue(strpos($response , 'Personal Information') !== false) ;
      $response = $this->call('GET' , 'viewstudent/fdsfd') ;
      $this->assertTrue(strpos($response , 'Bad data entered') !== false) ;
      $response = $this->call('GET' , 'viewstudent/1458') ;
      $this->assertTrue(strpos($response , 'Bad data entered') !== false) ;
      $response = $this->call('POST' , '/search_results') ;
      $this->assertTrue(strpos($response , 'Bad data entered') !== false) ;
      $response = $this->call('POST' , '/search_results' , ['text' => 'search text']) ;
      $this->assertTrue(strpos($response , 'Search Results') !== false) ;
      $response = $this->call('GET' , '/print/fsdfsdf' , ['text' => 'search text']) ;
      $this->assertTrue(strpos($response , 'Bad data entered') !== false) ;
      $response = $this->call('GET' , '/print/40256' , ['text' => 'search text']) ;
      $this->assertTrue(strpos($response , 'Bad data entered') !== false) ;
      $response = $this->call('GET' , '/departments/mech') ;
      $this->assertTrue(strpos($response->content() , 'Courses') !== false) ;
      $response = $this->call('GET' , '/departments/mechdsadas' ) ;
      $this->assertTrue(strpos($response->content() , 'Bad data entered') !== false) ;
      $response = $this->call('GET' , '/course/course_7' ) ;
      $this->assertTrue(strpos($response->content() , 'Average grade') !== false) ;
      $response = $this->call('GET' , '/course/course_745' ) ;
      $this->assertTrue(strpos($response->content() , 'Bad data entered') !== false) ;
    }
}
