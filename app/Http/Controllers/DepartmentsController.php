<?php

namespace App\Http\Controllers ;

use App\Http\Controllers\Controller ;
use Illuminate\Http\Request ;
use Illuminate\Http\Response ;
use App\User ;
use App\CoursesModel ;
use App\StPersonalModel ;

class DepartmentsController extends Controller
{
  public function show(Request $request , $dept)
  {
    if(! \Auth::check())
      return view('welcome' , ['error' => 'You need to login to view this information .']) ;
    else
    {
      if(count(StPersonalModel::where('dept' , '=' , $dept)->get()) == 0)
        return view('welcome' , ['error' => 'Bad data entered .']) ;
      $sems = [] ;
      for($sem = 1 ; $sem <= 8 ; $sem++)
      {
        $courses = [] ;
        $c_names = [] ;
        $ress = CoursesModel::where('dept' , '=' , $dept)->where('semenster' , '=' , $sem)->get() ;
        foreach($ress as $val)
        {
          if(!in_array($val->course_name , $c_names))
          {
            $courses[] = $val ;
            $c_names[] = $val->course_name ;
          }
        }
        $sems[$sem] = $courses ;
      }
      $base = 2013 ;
      $last = date("Y") ;
      $data = [] ;
      $courses_list = [] ;
      $all_list = CoursesModel::all() ;
      foreach($all_list as $c)
        if(!in_array($c->course_num , $courses_list))
          $courses_list[] = $c->course_num ;
      sort($courses_list) ;
      for($i = $base ; $i <= $last ; $i++)
      {
        $res = StPersonalModel::where('dept' , '=' , $dept)->where('passing_year' , '=' , $i)->get() ;
        $data[$i] = $res ;
      }
      return view('departments' , ['sems' => $sems , 'dept' => $dept , 'base' => $base , 'last' => $last , 'data' => $data , 'user' => \Auth::user() , 'courses' => $courses_list]);
    }
  }
}

?>
