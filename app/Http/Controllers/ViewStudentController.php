<?php

namespace App\Http\Controllers ;

use App\Http\Controllers\Controller ;
use Illuminate\Http\Request;
use Illuminate\Http\Response ;
use \Auth ;
use App\StPersonalModel ;
use App\StAcadModel ;
use App\StOtherModel ;
use App\CoursesModel ;
use App\SpiTableModel ;

class ViewStudentController extends Controller
{
  public function viewStudent(Request $request , $roll_no)
  {
    if(Auth::check())
    {
      if(!is_numeric($roll_no) || (count(StPersonalModel::where('roll_no' , '=' , intval($roll_no))->get()) == 0))
        return view('welcome' , ['error' => 'Bad data entered .']) ;
      $user = Auth::user() ;
      $past_arr = explode(',' , $user->past) ;
      if(! in_array($roll_no , $past_arr))
      {
        $string = $roll_no . ',' ;
        for($i=0 ; $i < 4 ; $i++)
        {
          $string = $string . $past_arr[$i] ;
          if($i != 3)
            $string = $string . ',' ;
        }
        echo $string ;
        $user->past = $string ;
        $user->save() ;
      }
      $st_personal = StPersonalModel::where('roll_no' , '=' , intval($roll_no))->first() ;
      $st_acad = StAcadModel::where('roll_no' , '=' , intval($roll_no))->first() ;
      $st_other = StOtherModel::where('roll_no' , '=' , intval($roll_no))->first() ;
      $spis = SpiTableModel::where('roll_no' , '=' , intval($roll_no))->get() ;
      $courses = CoursesModel::where('roll_no' , '=' , intval($roll_no))->get() ;
      $spi = [] ;
      foreach($spis as $ss)
        $spi[$ss->semester] = $ss->spi ;

        $courses_list = [] ;
        $all_list = CoursesModel::all() ;
        foreach($all_list as $c)
          if(!in_array($c->course_num , $courses_list))
            $courses_list[] = $c->course_num ;
        sort($courses_list) ;
      return view('studentinfo' , ['user' => $user , 'student_personal' => $st_personal , 'student_acad' => $st_acad , 'student_other' => $st_other,
            'spi' => $spi , 'courses' => $courses , 'c_list' => $courses_list]) ;
    }
    else
      return redirect('/') ;
  }
}

 ?>
