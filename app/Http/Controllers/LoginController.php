<?php
namespace App\Http\Controllers ;

use App\Http\Controllers\Controller ;
use Illuminate\Http\Request;
use Illuminate\Http\Response ;
use App\User ;
use \DB ;
use \Auth ;

class LoginController extends Controller
{
    public function loginValidate(Request $request)
    {
      $username = $request ->input('username') ;
      $password = $request ->input('password') ;
      $server = $request ->input('server') ;
      $rem = $request->input('remember_me') ;
      if(Auth::attempt(['name' => $username , 'password' => $password , 'server' => $server]))
        if($rem === "true")
          return (new Response(json_encode(new Result(true , route('dashboard') , "" , $rem))))->withCookie(cookie()->forever('name' , $username)) ;
        else
          return (new Response(json_encode(new Result(true , route('dashboard') , "" , $rem)))) ;
      else
      {
        $error = new Result(false , "" , "Incorrect login information !" , false) ;
        return new Response(json_encode($error)) ;
      }
  }
}

class Result
{
    public $success = true ;
    public $redirect_link = "" ;
    public $error = "" ;
    public $rem = false ;
    function __construct($s , $r , $e , $re)
    {
      $this ->success = $s ;
      $this->redirect_link = $r ;
      $this->error = $e ;
      $this->rem = $re ;
    }
}

 ?>
