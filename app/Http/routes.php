<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::get('/sis' , function(){
      if(Auth::check())
        return redirect()->route('dashboard');
      if(Request::cookie('name') != null)
      {
        if(! Auth::check())
        {
          $user = App\User::where('name' , '=' , Request::cookie('name'))->first();
          Auth::login($user) ;
        }
          return redirect()->route('dashboard') ;
      }
      else
        return view('login') ;
    });
    Route::get('/logout' , ['as' => 'logout' , 'uses' => 'LogoutController@logoutUser']) ;
    Route::post('/validate' , ['as' => 'login_validate' , 'uses' => 'LoginController@loginValidate']) ;
    Route::get('/dashboard' , ['as' => 'dashboard' , 'uses' => 'PostLoginController@showDashBoard']) ;
    Route::get('/dashboardinfo/{dept?}/{year?}' , ['as' => 'dashboardinfo' , 'uses' => 'DashboardInfoController@studentInfo']) ;
    Route::get('/viewstudent/{roll_no}' , ['as' => 'viewst' , 'uses' => 'ViewStudentController@viewStudent']);
    Route::post('/search_results' , ['as'=> 'search' , 'uses'=>'SearchController@search']) ;
    Route::get('/print/{roll_no}' , ['as'=> 'print' , 'uses'=>'PrintToPdfController@printStudent']) ;
    Route::get('/', function () {
      if(Request::cookie('name') != null)
      {
        if(! Auth::check())
        {
          $user = App\User::where('name' , '=' , Request::cookie('name'))->first();
          var_dump($user) ;
          echo Request::cookie('name') ;
          Auth::login($user) ;
        }
          return redirect()->route('dashboard') ;
      }
      else {
        if(!Auth::check())
          return view('welcome');
        else
          return redirect()->route('dashboard') ;
      }
    });
    Route::get('/departments/{dept}' , ['as' => 'departments' , 'uses' => 'DepartmentsController@show']);
    Route::get('/course/{course_num}' , ['as' => 'course' , 'uses' => 'CourseController@show']) ;
});
