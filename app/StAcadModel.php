<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StAcadModel extends Model
{
    //
    protected $table = 'st_acad' ;
    public $timestamps = false ;
}
