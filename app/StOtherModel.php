<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StOtherModel extends Model
{
    //
    protected $table = 'st_other' ;
    public $timestamps = false ;
}
