# Student Information System

## Purpose
The project provides a prototype of a student information system , which makes it
easy for the faculty to access relevant information about the student . This in-
formation helps the faculty to keep track of the student's academic performance
and also various other things . This information also gives the faculty a hint o-
f the reason for the low academic performance and/or other problems the student
is facing .

## Features
* Search for a student based on his/her name or roll number or e-mail .
* See basic details of last five students searched for .
* Generate a pdf file containing the details of the student in a single click .
* View all students of a particular department and an year at once .
* Keep me logged in feature .

## Technologies used :

* PHP (Laravel)
* HTML
* CSS(Bootstrap)
* Javascript
* Ajax
* MySQL

## Constraints

* The website assumes the information is stored in specific form in the database
tables . To change the structure of the database , the code should be changed .
* New students should be added manually .
* The server should have composer installed .
