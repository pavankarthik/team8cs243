<html>
<head>
  <title>Generate PDF</title>
  <style>
  body
  {
  	font-family: helvetica;
  }

  .pdf
  {
  	padding-left: 8%;
  	padding-top: 3%;
  	padding-right: 8%;
  	padding-bottom: 3%;
  }
  .personal_info
  {
  	padding-left: 2%;
  	width:100%;
  }


table {
  color: #333;
  font-family: sans-serif;
  font-size: .9em;
  font-weight: 300;
  text-align: left;
  line-height: 40px;
  border-spacing: 0;
  border: 2px solid #975997;
  width: 100%;
  margin: 50px auto;
}

thead tr:first-child {
  background: #975997;
  color: #fff;
  border: none;
}

th {font-weight: bold;}
th:first-child, td:first-child {padding: 0 15px 0 20px;}

thead tr:last-child th {border-bottom: 3px solid #ddd;}

tbody tr:hover {background-color: #ffeeff;}
tbody tr:last-child td {border: none;}
tbody td {border-bottom: 1px solid #ddd;}
th,td:first-child
{
	text-align: center;

}
th
{
	padding-right: 1%;
}
td {
	text-align: center;
}
.academic_info
{
	clear:both;
}


  </style>
</head>
<body>
	<div class="pdf">
		<div class="logo">
 		</div>

  		<div class="personal_info">
  			<table>
  				<thead>
    				<tr>
      					<th colspan="3">{{$st_personal->name}}</th>
    				</tr>
    				<tr>
      					<th>Department</th>
      					<th colspan="2" >{{$st_personal->dept}}</th>
    				</tr>
  				</thead>
  				<tbody>
    				<tr>
      					<td>Roll Number</td>
      					<td>{{$st_personal->roll_no}}</td>
    				</tr>
    				<tr>
      					<td>Date of Birth</td>
      					<td>{{$st_personal->dob}}</td>
    				</tr>
    				<tr>
      					<td>Year</td>
      					<td>{{$st_personal->year}}</td>
    				</tr>
    				<tr>
      					<td>Address</td>
      					<td>{{$st_personal->address}}</td>
    				</tr>
    				<tr>
      					<td>Contact</td>
      					<td>{{$st_personal->contact}}</td>
    				</tr>
    				<tr>
      					<td>E-mail Id</td>
      					<td>{{$st_personal->email}}</td>
    				</tr>
    				<tr>
      					<td>Father's Name</td>
      					<td>{{$st_personal->f_name}}</td>
    				</tr>
    				<tr>
      					<td>Father's Occupation</td>
      					<td>{{$st_personal->f_occ}}</td>
    				</tr>
    				<tr>
      					<td>Mother's Name</td>
      					<td>{{$st_personal->m_name}}</td>
    				</tr>
    				<tr>
      					<td>Mother's Occupation</td>
      					<td>{{$st_personal->m_occ}}</td>
    				</tr>
  				</tbody>
			</table>
  		</div>
  <div class="academic_info">
    <table>
      <tr>
        <thead>
        <tr>
            <th colspan="3">Other information</th>
        </tr>
      </thead>
        <th>Class</th>
        <th>School/College</th>
        <th>Grade/Percentage</th>
      </tr>
      <tr>
        <td>Tenth</td>
        <td>{{$st_acad->tenth_school}}</td>
        <td>{{$st_acad->tenth_gpi}}</td>
      </tr>
      <tr>
        <td>Twelth</td>
        <td>{{$st_acad->college}}</td>
        <td>{{$st_acad->inter_perc}}</td>
      </tr>
    </table>
	</div>


	<div class="spi">
  		<table>
  			<thead>
    			<tr>
      				<th colspan="3">Courses taken and grades</th>
    			</tr>
    			<tr>
      				<th>Course No.</th>
      				<th>Instructor</th>
      				<th>Credit</th>
    			</tr>
  			</thead>
  			<tbody>
          @for($i = 0 ; $i < count($courses ); $i++)
    			<tr>
      				<td>{{$courses[$i]->course_num}}</td>
      				<td>{{$courses[$i]->instructor}}</td>
      				<td>{{$courses[$i]->grade}}</td>
    			</tr>
          @endfor
    	  	</tbody>
		</table>
  	</div>



    <br><br>
<div class="other_info">
	<div class="info">
		<table>
      <thead>
      <tr>
          <th colspan="3">Other information</th>
      </tr>
    </thead>
			<tr>
				<td>Scholarships</td>
				<td>{{$st_acad->scholarships}}</td>
			</tr>
			<tr>
				<td>Faculty Advisor</td>
				<td>{{$st_acad->faculty_adv}}</td>
			</tr>
			<tr>
				<td>Medical Problems</td>
				<td>{{$st_other->med_prob}}</td>
			</tr>
			<tr>
				<td>Clubs</td>
				<td>{{$st_other->clubs}}</td>
			</tr>
		</table>
	</div>
</div>


  </div>
</div>
</body>
</html>
