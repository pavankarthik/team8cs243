<!DOCTYPE html>
<html>
    <script>
      interval = setInterval(changeText , 1000) ;
      var sec = 3 ;
      function changeText()
      {
        sec-- ;
        if(sec <= 0)
        {
          clearInterval(interval) ;
        }
        document.getElementById('seconds').innerHTML = 'Redirecting in ' + sec + ' secs.';
      }
    </script>
    <head>
       <meta http-equiv="refresh" content="3;url={{url('/sis')}}" />
        <title>Error</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
              @if(isset($error))
                <h2>{{$error}} </h2>
              @endif
                <h3 id = "seconds" >Redirecting in 3 secs. </h3>
            </div>
        </div>
    </body>
</html>
