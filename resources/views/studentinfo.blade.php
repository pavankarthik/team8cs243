

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="*Test Commit*">

    <title>{{$student_personal->name}} - SIS</title>

   <link rel="stylesheet" href="{{URL::asset('css/dashboard.css')}}">
<link rel="stylesheet" href="{{URL::asset('bootstrap/dist/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('bootstrap/dist/css/style.min.css')}}">
</head>
<body>
    <div id="wrapper">
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="navbar-header">

                <a class="navbar-brand" href="{{route('dashboard')}}">Student Information System</a>
      </div>
      <form method = "POST" action = {{route('search')}}>
        <input type="hidden" name="_token" value="{{ csrf_token() }}" id = "token" >
      <div class="navbar-left">
          <div class="input-group custom-search-form">
              <input type="text" class="form-control" placeholder="Search..." name = "text" required>
              <span class="input-group-btn">
                <button class="btn btn-default"  type="submit" >
                  <i class= "	glyphicon glyphicon-search" ></i>
                </button>
              </span>

          </div>
      </div>
    </form>
      <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span> {{$user->name}} <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><span class="glyphicon glyphicon-user"></span> Change Password</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="{{ route('logout') }}"><span class="glyphicon glyphicon-off"></span> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
          </nav>
      <div class="row">
        <div class="col-sm-3">
            <!-- Left column -->
            <a href="{{route('dashboard')}}"><strong><i class="glyphicon glyphicon-home"></i> Dashboard</strong></a>

            <hr>

            <ul class="nav nav-stacked">
                <li class="nav-header "> <a href="#" data-toggle="collapse" data-target="#userMenu">Departments </a>
                    <ul class="nav nav-stacked collapse" id="userMenu">
                        <li> <a data-toggle="collapse" data-target="#year_cse" href="{{route('departments' , ['dept' => 'mech'])}}" >&nbsp; Mechanical Engineering &nbsp;</a>
                        </li>
                        <li><a href="{{route('departments' , ['dept' => 'mech'])}}" data-toggle="collapse" data-target="#year_ece">&nbsp; Electronics and Communication Engineering </a>
                        </li>
                        <li><a href="{{route('departments' , ['dept' => 'mech'])}}" data-toggle="collapse" data-target="#year_eee">&nbsp; Electrical Engineering</a>
                        </li>
                        <li><a href="{{route('departments' , ['dept' => 'mech'])}}" data-toggle="collapse" data-target="#year_mnc">&nbsp; Department of Mathematics</a>
                        </li>
                        <li><a href="{{route('departments' , ['dept' => 'mech'])}}" data-toggle="collapse" data-target="#year_me">&nbsp; Computer Science and Engineering</a>
                        </li>
                        <li><a href="{{route('departments' , ['dept' => 'mech'])}}" data-toggle="collapse" data-target="#year_ce">&nbsp; Civil Engineering</a>
                          </li>
                        <li><a href="{{route('departments' , ['dept' => 'mech'])}}" data-toggle="collapse" data-target="#year_cst">&nbsp; Chemical Science and Technology</a>
                        </li>
                        <li><a href="{{route('departments' , ['dept' => 'mech'])}}" data-toggle="collapse" data-target="#year_c">&nbsp; Department of Chemistry</a>
                        </li>
                    </ul>
                </li>
                <li class="nav-header "> <a href="#" data-toggle="collapse" data-target="#menu2"> Courses </a>

                  <ul class="nav nav-stacked collapse" id="menu2">
                    @for($k = 0 ; $k < count($c_list) ; $k++)
                    <li> <a href="{{route('course' , ['course_num' => $c_list[$k]])}}">&nbsp; {{$c_list[$k]}}</a></li>
                    @endfor
                  </ul>
                </li>
            </ul>

        </div>

              <div class="col-sm-9">

                <p><h2 class="text-info">{{$student_personal->name}}</h2></p>
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-12 col-xs-offset-0 col-sm-offset-0  toppad" >


          <div class="panel panel-primary">
            <div class="panel-heading">
              <h2 class="panel-title">Personal Information</h2>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="{{URL::asset('images/User.png')}}" class="img-circle img-responsive"> </div>
                <div class=" col-md-9 col-lg-9 ">
                  <table class="table table-user-information">
                    <tbody>
                      <tr>
                        <td>Department:</td>
                        <td>{{$student_personal->dept}}</td>
                      </tr>
                      <tr>
                        <td>Roll Number</td>
                        <td>{{$student_personal->roll_no}}</td>
                      </tr>
                      <tr>
                        <td>Date of Birth</td>
                        <td>{{$student_personal->dob}}</td>
                      </tr>
                      <tr>
                        <td>Year</td>
                        <td>{{$student_personal->passing_year}}</td>
                      </tr>
                        <tr>
                        <td>Home Address</td>
                        <td>{{$student_personal->address}}</td>
                      </tr>
                      <tr>
                        <td>Email</td>
                        <td><a href="mailto:{{$student_personal->email}}">{{$student_personal->email}}</a></td>
                      </tr>
                      <tr>
                        <td>Phone Number</td>
                        <td>{{$student_personal->contact}}
                        </td>
                      </tr>
                      <tr>
                        <td>Father's Name</td>
                        <td>{{$student_personal->f_name}}
                        </td>
                      </tr>
                      <tr>
                        <td>Father's Occupation</td>
                        <td>{{$student_personal->f_occ}}
                        </td>
                      </tr>
                      <tr>
                        <td>Mothers's Occupation</td>
                        <td>{{$student_personal->m_occ}}
                        </td>
                      </tr>
                    </tbody>
                  </table>

                  <a href="{{route('print' , ['roll_no' => $student_personal->roll_no])}}" class="btn btn-primary">Generate PDF</a>
                </div>
              </div>
            </div>

          </div>
        </div>


        <div class="col-lg-12">
        <div class="callout callout-success">
                    <h3>Past Academic Performance</h3>
                  </div>
                </div>


        <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h3>{{$student_acad->tenth_gpi}}</h3>
                  <p>{{$student_acad->tenth_school}}</p>
                </div>

                <a href="#" class="small-box-footer">Tenth Standard <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->


            <div class="col-lg-3 col-xs-6">
                  <!-- small box -->
                  <div class="small-box bg-red">
                    <div class="inner">
                      <h3>{{$student_acad->inter_perc}}%</h3>
                      <p>{{$student_acad->college}}</p>
                    </div>

                    <a href="#" class="small-box-footer">Twelvth Standard <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
                </div><!-- ./col -->


                <div class="col-lg-3 col-xs-6">
                      <!-- small box -->
                      <div class="small-box bg-green">
                        <div class="inner">
                          <h3>{{$student_acad->adv_rank}}</h3>
                          <p>All India Rank</p>
                        </div>

                        <a href="#" class="small-box-footer">JEE Advanced <i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                    </div><!-- ./col -->


                    <div class="col-lg-3 col-xs-6">
                          <!-- small box -->
                          <div class="small-box bg-aqua">
                            <div class="inner">
                              <h3>{{$student_acad->mains_rank}}</h3>
                              <p>All India Rank</p>
                            </div>

                            <a href="#" class="small-box-footer">JEE Mains <i class="fa fa-arrow-circle-right"></i></a>
                          </div>
                        </div><!-- ./col -->

                        <div class="col-lg-12">
                        <div class="box box-solid box-primary">
                    			<div class="box-header">
                      				<h3 class="box-title">Academic Performance</h3>
                    			</div><!-- /.box-header -->
                    			<div class="box-body">


                            <div class="panel-body">
                            <h3>Cumulative Performance Index : <span class = "text-success">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$student_acad->cpi}}</span></h3>
                            <br>
                           	<div class="nav-tabs-custom">
                                      <ul class="nav nav-tabs">
                                        <li class="active"><a href="#tab_1" data-toggle="tab">Semester 1</a></li>
                                        @for($s = 2 ; $s <= 8 ; $s++)
                                        <li><a href="#tab_{{$s}}" data-toggle="tab">Semester {{$s}}</a></li>
                                        @endfor
                                      </ul>


                                      <div class="tab-content">
                                        @for($ss = 1 ; $ss <= 8 ; $ss++)
                                        <div class="tab-pane <?php if($ss == 1) echo ' active';?>" id="tab_{{$ss}}">

                                        	<div class="box">
                                      <div class="box-header">
                                        <h3 class="box-title">Semester {{$ss}}</h3>
                                      </div><!-- /.box-header -->
                                      <div class="box-body no-padding">
                                        <table class="table table-striped">
                                          <tr>
                                            <th style="width: 20%">Course No.</th>
                                            <th style="width:40%">Course Name</th>
                                            <th style="width:40%">Grade</th>
                                          </tr>
                                          <?php
                                          $sem = [] ;
                                          for($i = 0 ; $i < count($courses) ; $i++)
                                            if($courses[$i]->semenster == $ss)
                                              $sem[] = $courses[$i] ;
                                          ?>
                                            @for($i = 0 ; $i < count($sem) ; $i++)
                                            <tr>
                                              <td>{{$sem[$i]->course_num}}</td>
                                              <td><a href="#">{{$sem[$i]->course_name}}</a></td>
                                              <td><span class="badge bg-red">{{$sem[$i]->grade}}</span></td>
                                            </tr>
                                          @endfor
                                        </table>
                                        <hr>
                                        <h3>Semester Performance Index : <span class = "text-success">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$spi[$ss]}}</span></h3>
                                      </div><!-- /.box-body -->
                                    </div><!-- /.box -->
                                    </div><!-- /.tab-pane -->
                                      @endfor
                                      </div><!-- /.tab-content -->

                                    </div><!-- nav-tabs-custom -->
                                  </div>

                    			</div><!-- /.box-body -->
                  			</div><!-- /.box -->

                      </div>

                      <div class="col-lg-12">
                      <div class="box box-solid box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Other Information</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">

                          <table class="table table-striped">
                              <tr>
                                <td>Faculty Advisor</td>
                                <td>{{$student_acad->faculty_adv}}</td>
                              </tr>
                              <tr>
                                <td>Scholarships</td>
                                <td>{{$student_acad->scholarships}}</td>
                              </tr>
                              <tr>
                                <td>Projects</td>
                                <td>{{$student_acad->projects}}</td>
                              </tr>
                              <tr>
                                <td>Clubs</td>
                                <td>{{$student_other->clubs}}</td>
                              </tr>
                              <tr>
                                <td>Medical Problems</td>
                                <td>{{$student_other->med_prob}}</td>
                              </tr>
                          </table>



                        </div><!-- /.box-body -->
                      </div><!-- /.box -->

                    </div>

              </div>
            </div>
</div>


</body>
    <script src="{{URL::asset('bootstrap/js/tests/vendor/jquery.min.js')}}"></script>
    <script src="{{URL::asset('bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script>

      $(document).ready(function(){$(".alert").addClass("in").fadeOut(4500);

        /* swap open/close side menu icons */
      $('[data-toggle=collapse]').click(function(){
        // toggle icon
        $(this).find("i").toggleClass("glyphicon-chevron-right glyphicon-chevron-down");
      });
    });
    </script>
</html>


  <h2></h2>
  <h2></h2>
  <h2></h2>
  <h2></h2>
  <h2></h2>
  <h2></h2>
  <h2></h2>
  <h2></h2>
  <h2></h2>
  <h2></h2>
  <h2></h2>
  <h2></h2>
  <hr>
  <h1></h1>
  <h2></h2>
  <h2></h2>
