<html>
<head>
<title>Login Portal</title>
<link rel="stylesheet" href="{{URL::asset('css/main.css')}}">
<link rel="stylesheet" href="{{URL::asset('bootstrap/dist/css/bootstrap.min.css')}}">

</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <h1 class="text-center login-title">Student Information System IITG</h1>
            <div class="account-wall">
                <img class="profile-img" src="{{URL::asset('images/iitg_logo.png')}}" alt="">
                <form class="form-signin" method = "POST" action = {{route('login_validate')}} >
                <input type="hidden" name="_token" value="{{ csrf_token() }}" id = "token" >
                <input type="text" class="form-control" placeholder="User ID" required autofocus id = "username" name = "username" >
                <input type="password" class="form-control" placeholder="Password" required id = "password" name = "password" >
                <div class="btn-group">
                    <button name = "server" type="button" class="btn btn-default dropdown-toggle form-control" data-toggle="dropdown">
                    <span id = "login_server_name" data-bind="label">Login Server</span>&nbsp;<span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Namboor</a></li>
                        <li><a href="#">Disang</a></li>
                        <li><a href="#">Tambil</a></li>
                        <li><a href="#">Teesta</a></li>
                        <li><a href="#">Dikrong</a></li>
                    </ul>
                </div>
                <button class="btn btn-lg btn-primary btn-block" type="submit" id = "login_button" >
                    Sign in</button>
                <label class="checkbox pull-left">
                    <input type="checkbox" value="remember" name = "remember_me" id = "remember_me" >
                    Keep me logged in
                </label>
                <a href="#" class="pull-right need-help">Need help? </a><span class="clearfix"></span>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="animation">
  <img src="{{URL::asset('images/hourglass.svg')}}">
  <p>Validating Information</p>
</div>
</body>
    <script src="{{URL::asset('bootstrap/js/tests/vendor/jquery.min.js')}}"></script>
    <script src="{{URL::asset('bootstrap/dist/js/bootstrap.min.js')}}"></script>
  <script>
   $( document.body ).on( 'click', '.dropdown-menu li', function( event ) {

      var $target = $( event.currentTarget );

      $target.closest( '.btn-group' )
         .find( '[data-bind="label"]' ).text( $target.text() )
            .end()
         .children( '.dropdown-toggle' ).dropdown( 'toggle' );

      return false;

   });
  </script>
  <script>
  document.getElementById("login_button").addEventListener("click" , function(event){
    event.preventDefault();
    var xmlhttp = new XMLHttpRequest() ;
    xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                $(document).ajaxStart(function(){
                  body.addClass("loading");
                });
                var response = JSON.parse(xmlhttp.responseText) ;
                if(response.success == true)
                {
                  $(document).ajaxComplete(function(){
                    body.removeClass("loading");
                  });
                  location.reload() ;
                  console.log(response) ;
                }
                else
                {
                  $(document).ajaxComplete(function(){
                    body.removeClass("loading");
                  });
                    alert(response.error) ;
                }
            }
        };
    xmlhttp.open("POST" , "{{route('login_validate')}}" , true) ;
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    var formdata = 'username=' + document.getElementById("username").value ;
    formdata += "&" ;
    formdata += 'password=' + document.getElementById("password").value ;
    formdata += "&" ;
    formdata += 'server=' + document.getElementById("login_server_name").innerHTML ;
    formdata += "&" ;
    formdata += 'remember_me=' + document.getElementById("remember_me").checked ;
    formdata += "&" ;
    formdata += '_token=' + document.getElementById("token").value;
    console.log(formdata) ;
    xmlhttp.send(formdata);
  });
  </script>
</html>
