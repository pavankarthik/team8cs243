<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Search Results:{{$text}}</title>

   <link rel="stylesheet" href="{{URL::asset('css/dashboard.css')}}">
<link rel="stylesheet" href="{{URL::asset('bootstrap/dist/css/bootstrap.min.css')}}">
</head>

    <body>
      <div id="wrapper">
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="navbar-header">

                  <a class="navbar-brand" href="{{route('dashboard')}}">Student Information System</a>
        </div>
        <form method = "POST" action = {{route('search')}}>
          <input type="hidden" name="_token" value="{{ csrf_token() }}" id = "token" >
        <div class="navbar-left">
            <div class="input-group custom-search-form">
                <input type="text" class="form-control" value="{{$text}}" name = "text" required>
                <span class="input-group-btn">
                  <button class="btn btn-default"  type="submit" >
                    <i class= "	glyphicon glyphicon-search" ></i>
                  </button>
                </span>

            </div>
        </div>
      </form>
        <ul class="nav navbar-right top-nav">
                  <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span> {{$user->name}} <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                          <li>
                              <a href="#"><span class="glyphicon glyphicon-user"></span> Profile</a>
                          </li>
                          <li>
                              <a href="#"><span class="glyphicon glyphicon-envelope"></span> Inbox</a>
                          </li>
                          <li>
                              <a href="#"><span class="glyphicon glyphicon-cog"></span> History</a>
                          </li>
                          <li class="divider"></li>
                          <li>
                              <a href="{{ route('logout') }}"><span class="glyphicon glyphicon-off"></span> Log Out</a>
                          </li>
                      </ul>
                  </li>
              </ul>
            </nav>
        <div class="row">
          <div class="col-sm-3">
              <!-- Left column -->
              <a href="{{route('dashboard')}}"><strong><i class="glyphicon glyphicon-home"></i> Dashboard</strong></a>

              <hr>

              <ul class="nav nav-stacked">
                  <li class="nav-header "> <a href="#" data-toggle="collapse" data-target="#userMenu">Departments </a>
                      <ul class="nav nav-stacked collapse" id="userMenu">
                          <li> <a data-toggle="collapse" data-target="#year_cse" href="#" >&nbsp; Mechanical Engineering &nbsp;</a>
                          </li>
                          <li><a href="#" data-toggle="collapse" data-target="#year_ece">&nbsp; Electronics and Communication Engineering </a>
                          </li>
                          <li><a href="#" data-toggle="collapse" data-target="#year_eee">&nbsp; Electrical Engineering</a>
                          </li>
                          <li><a href="#" data-toggle="collapse" data-target="#year_mnc">&nbsp; Department of Mathematics</a>
                          </li>
                          <li><a href="#" data-toggle="collapse" data-target="#year_me">&nbsp; Computer Science and Engineering</a>
                          </li>
                          <li><a href="#" data-toggle="collapse" data-target="#year_ce">&nbsp; Civil Engineering</a>
                            </li>
                          <li><a href="#" data-toggle="collapse" data-target="#year_cst">&nbsp; Chemical Science and Technology</a>
                          </li>
                          <li><a href="#" data-toggle="collapse" data-target="#year_c">&nbsp; Department of Chemistry</a>
                          </li>
                      </ul>
                  </li>
                  <li class="nav-header "> <a href="#" data-toggle="collapse" data-target="#menu2"> Courses </a>

                    <ul class="nav nav-stacked collapse" id="menu2">
                      @for($i = 0 ; $i < count($courses) ; $i++)
                      <li> <a href="{{route('course' , ['course_num' => $courses[$i]])}}">&nbsp; {{$courses[$i]}}</a></li>
                      @endfor
                    </ul>
                  </li>
                  <li class="nav-header">
                      <a href="#" data-toggle="collapse" data-target="#menu3"> Gymkhana Council </a>
                      <ul class="nav nav-stacked collapse" id="menu3">
                          <li><a href=""><span class="glyphicon glyphicon-circle"></span> Club 1</a></li>
                          <li><a href=""><i class="glyphicon glyphicon-circle"></i> Club 2</a></li>
                          <li><a href=""><i class="glyphicon glyphicon-circle"></i> Club 3</a></li>
                      </ul>
                  </li>
              </ul>

          </div>
          <div class="col-sm-9">
          <div class="col-sm-2">
          </div>
          <div class="col-sm-7">
                  <h2><p class="text-info">Search Results - Student Information System IITG</p></h2>
          </div>
        </div>
        <div class="col-sm-9">
          <div class="col-sm-2">
          </div>
          <div class="col-sm-7">
          <h3><p class="text-success">Showing {{count($array)}} Results for <b>{{$text}}<b></p></h3>
          </div>
        </div>
          <div class="col-sm-9">
        <div class="row">

        @for($i = 0 ; $i < count($array) ; $i++)
           <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 col-xs-offset-0 col-sm-offset-0 col-md-offset-1 col-lg-offset-1 toppad" >


        <div class="panel panel-info">
          <div class="panel-heading">
            <h3 class="panel-title">{{$array[$i]->name}}</h3>
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="{{URL::asset('images/User.png')}}" class="img-circle img-responsive"> </div>
              <div class=" col-md-9 col-lg-9 ">
                <table class="table table-user-information">
                  <tbody>
                    <tr>
                      <td>Department:</td>
                      <td>{{$array[$i]->dept}}</td>
                    </tr>
                    <tr>
                      <td>Roll Number:</td>
                      <td>{{$array[$i]->roll_no}}</td>
                    </tr>
                    <tr>
                      <td>Year</td>
                      <td>{{$array[$i]->year}}</td>
                    </tr>
                    <tr>
                      <td>Email</td>
                      <td><a href="mailto:{{$array[$i]->email}}">{{$array[$i]->email}}</a></td>
                    </tr>
                    <tr>
                      <td>Phone Number</td>
                      <td>{{$array[$i]->contact}}
                      </td>

                    </tr>

                  </tbody>
                </table>


                <a href="{{route('viewst' , ['roll_no' => $array[$i]->roll_no])}}" class="btn btn-primary">View More</a>&nbsp;&nbsp;&nbsp;
                <a href="#" class="btn btn-primary">Generate PDF</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      @endfor
    </div>
  </div>





              </div>
  </div>




</body>
    <script src="{{URL::asset('bootstrap/js/tests/vendor/jquery.min.js')}}"></script>
    <script src="{{URL::asset('bootstrap/dist/js/bootstrap.min.js')}}"></script>
</html>
