<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourse1Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->integer('roll_no');
            $table->foreign('roll_no')->references('roll_no')->on('st_personal')->onDelete('cascade') ;
            $table->string('grade') ;
            $table->string('instructor') ;
            $table->integer('year');
            $table->string('course_num') ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('courses');
    }
}
