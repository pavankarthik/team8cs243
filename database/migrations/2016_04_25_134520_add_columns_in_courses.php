<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsInCourses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('courses', function (Blueprint $table) {
            //
            $table->string('course_name') ;
            $table->tinyInteger('l') ;
            $table->tinyInteger('t') ;
            $table->tinyInteger('p') ;
            $table->tinyInteger('c') ;
            $table->string('dept') ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('courses', function (Blueprint $table) {
            //
            $table->dropColumn('course_name') ;
            $table->dropColumn('l') ;
            $table->dropColumn('t') ;
            $table->dropColumn('p') ;
            $table->dropColumn('c') ;
            $table->dropColumn('dept') ;
        });
    }
}
