<?php

use Illuminate\Database\Seeder;
use App\User ;
use App\StPersonalModel ;
use App\StAcadModel ;
use App\StOtherModel ;
use App\CoursesModel ;
use App\SpiTableModel ;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *d
     * @return void
     */
    public function run()
    {
        $this->call(StAcadTableSeeder::class) ;
         $this->command->info('Students acad table seeded .');
        $this->call(StOtherTableSeeder::class) ;
         $this->command->info('Students others table seeded .');
         $this->call(UserTableSeeder::class) ;
         $this->command->info('Users Table seeded .');
         $this->call(StPersonalTableSeeder::class) ;
         $this->command->info('Students Personal Table seeded .');
        $this->call(CoursesTableSeeder::class) ;
        $this->command->info('Courses table seeded .') ;
         $this->call(SpiTableSeeder::class) ;
         $this->command->info('Spi table seeded .') ;

    }
}

class UserTableSeeder extends Seeder
{
  public function run()
  {
    User::create(['name' => 'yashwanthbetha' , 'password' => Hash::make('appocalypse007') , 'server' => 'Disang', 'past' => '401,302,102,306,109']) ;
    User::create(['name' => 'mahesh' , 'password' => Hash::make('chemilla') , 'server' => 'Disang' , 'past' => '401,302,102,306,109']) ;
    User::create(['name' => 'kenkaneki' , 'password' => Hash::make('tokyoghoul') , 'server' => 'Disang' , 'past' => '401,302,102,306,109']) ;
    User::create(['name' => 'kurosakiichigo' , 'password' => Hash::make('bleach16') , 'server' => 'Disang' , 'past' => '401,302,102,306,109']) ;
    User::create(['name' => 'minatonamikaze' , 'password' => Hash::make('narutoshippuden') , 'server' => 'Disang' , 'past' => '401,302,102,306,109']) ;
    User::create(['name' => 'george' , 'password' => Hash::make('george') , 'server' => 'Disang' , 'past' => '401,302,102,306,109']) ;
  }
}

class StPersonalTableSeeder extends Seeder
{
  public function run()
  {
    $base = 2012 ;
    for($year = 1 ; $year <= 4 ; $year++)
    {
      $base ++ ;
      for($student = 1 ; $student <= 9 ; $student++)
      {
        if($year !== $student)
        {
        STPersonalModel::create([ 'roll_no' => $year*100 + $student , 'name' => "student_m{$year}{$student}" , 'dob' => '199'.(4-$year)."-{$student}-" . ($student+$year),
                                'dept' => 'mech' , 'year' => $year , 'address' => "Door No : d{$student}{$year},Street No : s{$student}{$year} , State : st{$student}" ,
                                'contact' => "9{$year}8{$year}5{$year}9{$student}4{$student},9{$year}8{$student}5{$year}9{$year}4{$student}" , 'email' => "studentm{$year}{$student}@iitg.ernet.in",
                                'f_name' => "father_name_m{$year}{$student}" , 'm_name' => "mother_name_m{$year}{$student}" , 'f_occ' => "father_occupation_m{$year}{$student}",
                                'm_occ' => "mother_occupation_m{$year}{$student}" , 'passing_year' => $base]) ;
        }
      }
    }
  }
}

class StAcadTableSeeder extends Seeder
{
  public function run()
  {
    for($year = 1 ; $year <= 4 ; $year++)
    {
      for($student = 1 ; $student <= 9 ; $student++)
      {
        if($year !== $student)
        {
        StAcadModel::create([ 'roll_no' => $year*100 + $student , 'mains_rank' => strval($year*100 + $student+1) , 'adv_rank' => strval($year*7+$student*5),
                                'inter_perc' => strval(($year*13 + $student*7) % 100) , 'college' => "College_m{$student}" , 'tenth_gpi' => strval(($year*19+$student*29)/10),
                                'tenth_school' => "School m{$student}{$year}" , 'courses' => "Courses list {$student}{$year}" , 'spi_list' => "SPI list {$student}{$year}",
                                'cpi' => strval(($year*79 + $student*47)/10) , 'backlogs' => strval("No backlogs") , 'scholarships' => 'Scholarships , if any',
                                'faculty_adv' => "Faculty Advisor {$student}{$year}" , 'projects' => "All projects done by him ."]) ;
        }
      }
    }
  }
}

class StOtherTableSeeder extends Seeder
{
  public function run()
  {
    for($year = 1 ; $year <= 4 ; $year++)
    {
      for($student = 1 ; $student <= 9 ; $student++)
      {
        if($year !== $student)
        {
        StOtherModel::create([ 'roll_no' => $year*100 + $student , 'clubs' => 'Club 1 , Club 2 , Club 3' , 'med_prob' => 'Medical problems , if any .']) ;
        }
      }
    }
  }
}

class CoursesTableSeeder extends Seeder
{
  public function run()
  {
    $grades = ['AA' , 'AB' , 'BB' , 'BC', 'CC' , 'CD' , 'DD'] ;
    $len = count($grades) ;
    for($sem = 1 ; $sem <= 8 ; $sem++)
    {
      for($year = 1 ; $year <= 4 ; $year++)
      {
        for($student = 1 ; $student <= 9 ; $student++)
        {
          if($year !== $student)
          {
          $c = rand(0,8) ;
          $l = rand() % 6 ;
          $t = rand() % 6 ;
          $p = rand() % 6 ;
          $cr = 2*$l + $t + $p ; ;
          CoursesModel::create(['roll_no' => $year*100 + $student , 'grade' => strval($grades[rand(0 , count($grades) - 1)]) , 'instructor' => 'instructor_'.strval(rand(0 , 1000)) ,
                                'year' => rand(1950,2099) , 'course_num' => 'course_' . $c , 'semenster' => $sem , 'course_name' => 'Name of course ' . $c,
                                'l' => $l , 't' => $t , 'p' => $p , 'c' => $cr , 'dept' => 'mech']) ;
          }
        }
      }
    }
  }
}

class SpiTableSeeder extends Seeder
{
  public function run()
  {
    for($sem = 1 ; $sem <= 8 ; $sem++)
    {
      for($year = 1 ; $year <= 4 ; $year++)
      {
        for($student = 1 ; $student <= 9 ; $student++)
        {
          if($year !== $student)
          {
          SpiTableModel::create(['roll_no' => $year*100 + $student , 'spi' => (rand()/getrandmax())*10 , 'semester' => $sem ]) ;
          }
        }
      }
    }
  }
}
